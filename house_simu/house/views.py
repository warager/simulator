from django.contrib import auth
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login
from django.http import JsonResponse
from django.shortcuts import render, redirect
from house_simu.house.helpers import create_initial_state
from house_simu.house.models import Item, Room


def main(request):
    """
    Renders start page
    """
    return render(request, 'house/main.html')


def signup(request):
    """
    Site registration function
    """
    context = {
        'email': request.POST.get('email', ""),
        'password': request.POST.get('password', ""),
        'pass_conf': request.POST.get('confirm', ""),
        'username': request.POST.get('username', "")
    }

    if User.objects.filter(username=context['email']).count():
        return JsonResponse({'success': False, 'error': 'error'})
    if context['email'] == "":
        return JsonResponse({'success': False, 'error': 'error'})
    if context['password'] == "":
        return JsonResponse({'success': False, 'error': 'error'})
    if context['password'] != context['pass_conf']:
        return JsonResponse({'success': False, 'error': 'error'})

    # creating user
    user = User.objects.create_user(
        username=context['email'],
        email=context['email'],
        first_name=context['username']
    )
    user.set_password(context['password'])
    user.save()
    user.backend = "django.contrib.auth.backends.ModelBackend"
    auth_login(request, user)
    # creating initial configuration of simulation
    create_initial_state(user)

    return JsonResponse({'success': True})


def signin(request):
    """
        Site log in function
    """
    context = {
        'email': request.POST.get('email', ""),
        'password': request.POST.get('password', "")
    }

    try:
        user = User.objects.get(email__iexact=context['email'], is_active=True)
    except User.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'error'})

    if not user.check_password(context['password']):
        return JsonResponse({'success': False, 'error': 'error'})

    user.backend = "django.contrib.auth.backends.ModelBackend"
    auth_login(request, user)

    return JsonResponse({'success': True})


@login_required
def signout(request):
    """
        Site logout function
    """
    auth.logout(request)
    return redirect('/')


def in_the_room(request):
    user = request.user
    if not user.is_authenticated():
        return redirect('/')

    room = user.room_set.all()[0]
    data = {
        'user': user.first_name,
        'room': room.name
    }
    return render(request, "house/in_the_room.html", data)


def get_room_items(request):
    rows = list()
    user = request.user
    room = user.room_set.all()[0]
    items = Item.objects.filter(room=room)

    for item in items:
        rows.append({
            'id': item.id,
            'name': item.name
        })

    return JsonResponse({'rows': rows})


def add_room_item(request):
    name = request.POST.get('name', '')
    user = request.user

    if not user.is_authenticated():
        return redirect('/')
    room = user.room_set.all()[0]

    try:
        Item.objects.create(room=room, name=name)
    except ValueError:
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})


def get_user_items(request):
    rows = list()
    user = request.user
    items = Item.objects.filter(user=user)

    for item in items:
        rows.append({
            'id': item.id,
            'name': item.name
        })

    return JsonResponse({'rows': rows})


def add_user_item(request):
    name = request.POST.get('name', '')
    user = request.user

    if not user.is_authenticated():
        return redirect('/')

    try:
        Item.objects.create(user=user, name=name)
    except ValueError:
        return JsonResponse({'success': False})
    else:
        return JsonResponse({'success': True})


def take_item(request):
    item_id = request.POST.get('id', '')
    user = request.user

    try:
        item = Item.objects.get(pk=item_id)
    except Item.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'does_not_exist'})
    else:
        item.room = None
        item.user = user
        item.save()

    return JsonResponse({'success': True})


def drop_item(request):
    item_id = request.POST.get('id', '')
    user = request.user
    room = user.room_set.all()[0]

    try:
        item = Item.objects.get(pk=item_id)
    except Item.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'does_not_exist'})
    else:
        item.user = None
        item.room = room
        item.save()

    return JsonResponse({'success': True})


def delete_item(request):
    item_id = request.POST.get('id', '')
    try:
        Item.objects.get(pk=item_id).delete()
    except Item.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'does_not_exist'})
    return JsonResponse({'success': True})


def get_rooms_list(request):
    rows = list()
    rooms = Room.objects.all()

    for room in rooms:
        rows.append(
            room.name
        )

    return JsonResponse({'rows': rows})


def select_room(request):
    user = request.user
    room_name = ''
    context = {
        'room': request.POST.get('room', '')
    }
    try:
        room = Room.objects.get(user=user)
    except Room.DoesNotExist:
        JsonResponse({'success': False, 'error': 'does_not_exist'})
    else:
        room.user = None
        room.save()
        try:
            new_room = Room.objects.get(name=context['room'])
        except Room.DoesNotExist:
            JsonResponse({'success': False, 'error': 'does_not_exist'})
        else:
            new_room.user = user
            new_room.save()
            room_name = new_room.name

    return JsonResponse({'success': True, 'room': room_name})