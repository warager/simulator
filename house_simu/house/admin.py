from django.contrib import admin
from house_simu.house.models import Item, Room


class ItemsAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'room', 'user']


class RoomsAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'name']


admin.site.register(Item, ItemsAdmin)
admin.site.register(Room, RoomsAdmin)