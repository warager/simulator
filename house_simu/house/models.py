from django.contrib.auth.models import User
from django.db import models


class Room(models.Model):
    user = models.ForeignKey(User, null=True, default=None)
    name = models.CharField(max_length=100)


class Item(models.Model):
    name = models.CharField(max_length=255)
    room = models.ForeignKey(Room, null=True, default=None)
    user = models.ForeignKey(User, null=True, default=None)

    class Meta:
        unique_together = (("id", "room"), ('id', 'user'))

