/*!
 * Ext JS Library 3.4.0
 * Copyright(c) 2006-2011 Sencha Inc.
 * licensing@sencha.com
 * http://www.sencha.com/license
*/
Ext.onReady(function () {
  Ext.QuickTips.init();

  var roomItemsStore = new Ext.data.JsonStore({
    // store configs
    autoLoad: true,
    autoDestroy: true,
    url: '/get_room_items',
    storeId: 'roomItemsStore',
    // reader configs
    root: 'rows',
    idProperty: 'id',
    totalProperty: 'total',
    remoteSort: false,
    fields: [
      'name',
      'id'
    ],
    writer: 'json',
    sortInfo: {field: 'name', direction: 'ASC'}
  });

  var userItemsStore = new Ext.data.JsonStore({
    // store configs
    autoLoad: true,
    autoDestroy: true,
    url: '/get_user_items',
    storeId: 'userItemsStore',
    // reader configs
    root: 'rows',
    idProperty: 'id',
    totalProperty: 'total',
    remoteSort: false,
    fields: [
      'name',
      'id'
    ],
    writer: 'json',
    sortInfo: {field: 'name', direction: 'ASC'}
  });

  var roomItemsCm = new Ext.grid.ColumnModel({
    defaults: {
      sortable: true
    },
    columns: [
      {
        header: "Item's name",
        dataIndex: 'name',
        width: 180
      },
      {
        id: 'id',
        header: "Item's id",
        dataIndex: 'id',
        width: 180,
        hidden: true
      },
      {
        header: 'Delete item',
        width: 70,
        sortable: false,
        renderer: function (){
            return '<button class="btn-delete">Delete</button>'
        },
        dataIndex: 'actionDelete'
      },
      {
        header: 'Take item',
        width: 70,
        sortable: false,
        renderer: function (){
            return '<button class="btn-take">Take</button>'
        },
        dataIndex: 'actionTake'
      }
    ]
  });


  var userItemsCm = new Ext.grid.ColumnModel({
    defaults: {
      sortable: true
    },
    columns: [
      {
        header: "Item's name",
        dataIndex: 'name',
        width: 180
      },
      {
        id: 'id',
        header: "Item's id",
        dataIndex: 'id',
        width: 180,
        hidden: true
      },
      {
        header: 'Delete item',
        width: 70,
        sortable: false,
        renderer: function (){
            return '<button class="btn-delete">Delete</button>'
        },
        dataIndex: 'actionDelete'
      },
      {
        header: 'Drop item',
        width: 70,
        sortable: false,
        renderer: function (){
            return '<button class="btn-drop">Drop</button>'
        },
        dataIndex: 'actionDrop'
      }
    ]
  });

  var roomItemsGrid = new Ext.grid.GridPanel({
    id: 'roomItems',
    store: roomItemsStore,
    cm: roomItemsCm,
    renderTo: 'room-items-grid',
    width: 400,
    height: 150,
    stripeRows: true,
    title: 'Items in the room',
    frame: true,
    clicksToEdit: 1,
    tbar: [
      {
        text: 'Add new item',
        handler: createRoomItem
      }
    ],
    listeners: {
      scope: this,
      rowclick: actionWithItem
    }
  });

  var userItemsGrid = new Ext.grid.GridPanel({
    id: 'userItems',
    store: userItemsStore,
    cm: userItemsCm,
    renderTo: 'user-items-grid',
    width: 400,
    height: 150,
    stripeRows: true,
    title: "Items in user's pockets",
    frame: true,
    clicksToEdit: 1,
    tbar: [
      {
        text: 'Add new item',
        handler: createUserItem
      }
    ],
    listeners: {
      scope: this,
      rowclick: actionWithItem
    }
  });
});