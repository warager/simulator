$(document).on('click', '.getRoomsList', getRoomsList);

function getRoomsList() {
  var form = new Ext.form.FormPanel({
    baseCls: 'x-plain',
    labelWidth: 55,
    url: '/select_room',
    layout: {
      type: 'vbox',
      align: 'stretch'
    },
    defaults: {
      xtype: 'textfield'
    },
    items: [
      {
        xtype: 'combo',
        store: ['Hall', 'Kitchen', 'Bathroom', 'Bedroom' ],
        plugins: [Ext.ux.FieldLabeler ],
        fieldLabel: 'Room',
        dataIndex: 'room',
        name: 'room'
      }
    ]
  });

  var w = new Ext.Window({
    title: 'Select room',
    collapsible: true,
    maximizable: true,
    width: 300,
    height: 100,
    minWidth: 300,
    minHeight: 100,
    layout: 'fit',
    plain: true,
    bodyStyle: 'padding:5px;',
    buttonAlign: 'center',
    items: form,
    buttons: [
      {
        text: "Select",
        handler: function () {
          form.getForm().submit({
            url: '/select_room',
            waitMsg: 'Loading data...',
            success: function (form, action) {
              Ext.Msg.alert('Success', action.result.msg);
              Ext.getCmp('roomItems').getView().ds.reload();
              Ext.getCmp('userItems').getView().ds.reload();
              debugger;
              var resp_object = Ext.decode(action.response.responseText);
              $('#mySpan').text(resp_object['room']);
              w.close()
            },
            failure: function (form, action) {
              Ext.Msg.alert('Failed', action.result.msg);
            }
          })
        }
      },
      {
        text: 'Cancel',
        handler: function () {
          w.close()
        }
      }
    ]
  });
  w.show();
}