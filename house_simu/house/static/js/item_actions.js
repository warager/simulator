function createRoomItem() {
  var form = new Ext.form.FormPanel({
    baseCls: 'x-plain',
    labelWidth: 55,
    url: '/add_room_item',
    layout: {
      type: 'vbox',
      align: 'stretch'
    },
    defaults: {
      xtype: 'textfield'
    },
    items: [{
        plugins: [ Ext.ux.FieldLabeler ],
        fieldLabel: "Name",
        dataIndex: 'name',
        name: 'name'
      }]
  });

  var w = new Ext.Window({
    title: 'Create Room Item',
    collapsible: true,
    maximizable: true,
    width: 250,
    height: 100,
    minWidth: 250,
    minHeight: 100,
    layout: 'fit',
    plain: true,
    bodyStyle: 'padding:5px;',
    buttonAlign: 'center',
    items: form,
    buttons: [
      {
        text: "Save Item",
        handler: function () {
          form.getForm().submit({
            url: '/add_room_item',
            waitMsg: 'Loading data...',
            success: function (form, action) {
              debugger;
              Ext.Msg.alert('Success', action.result.msg);
              Ext.getCmp('roomItems').getView().ds.reload();
              w.close()
            },
            failure: function (form, action) {
              Ext.Msg.alert('Failed', action.result.msg);
            }
          })
        }
      },
      {
        text: 'Cancel',
        handler: function () {
          w.close()
        }
      }
    ]
  });
  w.show();
}

function createUserItem() {
  var form = new Ext.form.FormPanel({
    baseCls: 'x-plain',
    labelWidth: 55,
    url: '/add_user_item',
    layout: {
      type: 'vbox',
      align: 'stretch'
    },
    defaults: {
      xtype: 'textfield'
    },
    items: [{
        plugins: [ Ext.ux.FieldLabeler ],
        fieldLabel: "Name",
        dataIndex: 'name',
        name: 'name'
      }]
  });

  var w = new Ext.Window({
    title: 'Create User Item',
    collapsible: true,
    maximizable: true,
    width: 250,
    height: 100,
    minWidth: 250,
    minHeight: 100,
    layout: 'fit',
    plain: true,
    bodyStyle: 'padding:5px;',
    buttonAlign: 'center',
    items: form,
    buttons: [
      {
        text: "Save Item",
        handler: function () {
          form.getForm().submit({
            url: '/add_user_item',
            waitMsg: 'Loading data...',
            success: function (form, action) {
              debugger;
              Ext.Msg.alert('Success', action.result.msg);
              Ext.getCmp('userItems').getView().ds.reload();
              w.close()
            },
            failure: function (form, action) {
              Ext.Msg.alert('Failed', action.result.msg);
            }
          })
        }
      },
      {
        text: 'Cancel',
        handler: function () {
          w.close()
        }
      }
    ]
  });
  w.show();
}

$(document).on("pageload", function () {
  alert("pageload event fired!");
});

function actionWithItem(cpn, rowIndex, e) {
  var id = cpn.store.getAt(rowIndex).get('id');
  if (e.target.className.indexOf('btn-take') > -1) {
    var takeItem = function (btn) {
      console.info('You pressed ' + btn);
      if (btn == 'yes') {
        Ext.Ajax.request({
          url: '/take_item',
          success: function () {
            Ext.getCmp('roomItems').getView().ds.reload();
            Ext.getCmp('userItems').getView().ds.reload();
          },
          failure: function () {
            alert('Error in server');
          },
          params: {id: id}
        });
      }
    };
    Ext.MessageBox.show({
      title: 'Take item?',
      msg: 'You are taking this item. Continue?',
      buttons: Ext.MessageBox.YESNO,
      fn: takeItem,
      icon: Ext.MessageBox.QUESTION
    });
  }
  else if (e.target.className.indexOf('btn-drop') > -1) {
    var dropItem = function (btn) {
      console.info('You pressed ' + btn);
      if (btn == 'yes') {
        Ext.Ajax.request({
          url: '/drop_item',
          success: function () {
            Ext.getCmp('roomItems').getView().ds.reload();
            Ext.getCmp('userItems').getView().ds.reload();
          },
          failure: function () {
            alert('Error in server');
          },
          params: {id: id}
        });
      }
    };
    Ext.MessageBox.show({
      title: 'Drop item?',
      msg: 'You are dropping this item. Continue?',
      buttons: Ext.MessageBox.YESNO,
      fn: dropItem,
      icon: Ext.MessageBox.QUESTION
    });
  }
  else if (e.target.className.indexOf('btn-delete') > -1) {
    var delItem = function (btn) {
      console.info('You pressed ' + btn);
      if (btn == 'yes') {
        Ext.Ajax.request({
          url: '/delete_item',
          success: function () {
            Ext.getCmp('roomItems').getView().ds.reload();
            Ext.getCmp('userItems').getView().ds.reload();
          },
          failure: function () {
            alert('Error in server');
          },
          params: {id: id}
        });
      }
    };
    Ext.MessageBox.show({
      title: 'Delete item?',
      msg: 'You are deleting this item. Continue?',
      buttons: Ext.MessageBox.YESNO,
      fn: delItem,
      icon: Ext.MessageBox.QUESTION
    });
  }
}


