from house_simu.house.models import Room, Item


def create_item(name, room):
    try:
        Item.objects.get(name=name)
    except Item.DoesNotExist:
        Item.objects.create(name=name, room_id=room.pk)
    except Item.MultipleObjectsReturned:
        pass


def create_initial_state(user):

    # creating rooms
    hall, _ = Room.objects.get_or_create(name='Hall', user=user)
    kitchen, _ = Room.objects.get_or_create(name='Kitchen')
    bedroom, _ = Room.objects.get_or_create(name='Bedroom')
    bathroom, _ = Room.objects.get_or_create(name='Bathroom')

    # creating items
    Item.objects.create(name='iPhone', user=user)
    create_item('Umbrella', hall)
    create_item('Knife', kitchen)
    create_item('Plate', kitchen)
    create_item('Pillow', bedroom)
    create_item('Plaid', bedroom)
    create_item('Fan', bathroom)
    create_item('Shampoo', bathroom)