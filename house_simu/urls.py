from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns(
    '',
    url(r'^$', 'house_simu.house.views.main'),
    url(r'^signup$', 'house_simu.house.views.signup'),
    url(r'^signin$', 'house_simu.house.views.signin'),
    url(r'^signout$', 'house_simu.house.views.signout'),
    url(r'^in_the_room$', 'house_simu.house.views.in_the_room'),
    url(r'^get_room_items$', 'house_simu.house.views.get_room_items'),
    url(r'^add_room_item$', 'house_simu.house.views.add_room_item'),
    url(r'^get_user_items$', 'house_simu.house.views.get_user_items'),
    url(r'^add_user_item$', 'house_simu.house.views.add_user_item'),
    url(r'^take_item$', 'house_simu.house.views.take_item'),
    url(r'^drop_item$', 'house_simu.house.views.drop_item'),
    url(r'^delete_item$', 'house_simu.house.views.delete_item'),
    url(r'^get_rooms_list$', 'house_simu.house.views.get_rooms_list'),
    url(r'^select_room', 'house_simu.house.views.select_room'),
    url(r'^admin/', include(admin.site.urls)),
)